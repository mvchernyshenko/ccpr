package ru.hse.cppr.routing.handlers

import io.undertow.Handlers
import io.undertow.server.RoutingHandler
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import ru.hse.cppr.provider.AuthorizationProvider
import ru.hse.cppr.routing.dispatchers.SignUpDispatcher
import ru.hse.cppr.security.SecurityRoles.CPPW
import ru.hse.cppr.service.users.UsersService

object UsersHandler : KoinComponent{

    private val usersService: UsersService                         by inject()

    private val dispatcher = SignUpDispatcher(usersService)


    fun dispatch(): RoutingHandler =
        Handlers.routing()
            // User registartion routes

            .add("POST", "/signup/user", AuthorizationProvider.sessionWrapper(dispatcher::userRegistration, CPPW))
            .add("POST", "/signup/student", dispatcher::studentRegistration)
}
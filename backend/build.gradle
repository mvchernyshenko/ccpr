apply plugin: 'idea'
apply plugin: 'java'
apply plugin: 'kotlin'
apply plugin: 'kotlin-kapt'
apply plugin: 'application'
apply plugin: 'nu.studer.jooq'

sourceCompatibility = 1.8
targetCompatibility = 1.8

compileKotlin {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

compileTestKotlin {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

mainClassName   = "ru.hse.cppr.application.ApplicationKt"
applicationName = 'ccpr-backend'

repositories {
    maven { url "https://jitpack.io" }
    mavenCentral()
    maven { url "https://oss.sonatype.org/content/repositories/snapshots" }
}

jar {
    manifest {
        attributes 'Main-Class': "ru.hse.cppr.application.ApplicationKt"
    }

    from { configurations.compileClasspath.collect { it.isDirectory() ? it : zipTree(it) } }
}

test {
    useJUnitPlatform()

    dependsOn 'cleanTest'

    testLogging {
        events "passed", "skipped", "failed"
        exceptionFormat = 'full'
        showStandardStreams = true
    }
}

dependencies {
    // Core libraries
    implementation     "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version"
    implementation     "org.jetbrains.kotlin:kotlin-reflect:$kotlin_version"

    // Coroutines libraries
    implementation     "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version"


    implementation     "io.arrow-kt:arrow-core:$arrow_version"
    implementation     "io.arrow-kt:arrow-core-data:$arrow_version"
    implementation     "io.arrow-kt:arrow-generic:$arrow_version"
    implementation     "io.arrow-kt:arrow-optics:$arrow_version"
    implementation     "io.arrow-kt:arrow-optics-mtl:$arrow_version"
    implementation     "io.arrow-kt:arrow-fx:$arrow_version"
    implementation     "io.arrow-kt:arrow-fx-rx2:$arrow_version"
    implementation     "io.arrow-kt:arrow-syntax:$arrow_version"
    implementation     "io.arrow-kt:arrow-mtl:$arrow_version"
    implementation     "io.arrow-kt:arrow-mtl-data:$arrow_version"
    implementation     "io.arrow-kt:arrow-free:$arrow_version"
    implementation     "io.arrow-kt:arrow-free-data:$arrow_version"
    implementation     "io.arrow-kt:arrow-aql:$arrow_version"
    implementation     "io.arrow-kt:arrow-ui:$arrow_version"

    kapt               "io.arrow-kt:arrow-meta:$arrow_version"
    kapt               "io.arrow-kt:arrow-generic:$arrow_version"

    // Sugar libraries
    implementation     "joda-time:joda-time:$jodatime_version"

    // network libraries
    implementation     "com.sun.mail:javax.mail:$javax_mail_version"

    // http-libraries
    implementation     "io.undertow:undertow-core:$undertow_version"
    implementation     "io.undertow:undertow-servlet:$undertow_version"

    // parsing libraries
    implementation     "com.jsoniter:jsoniter:$jsoniter_version"

    // database & cache libraries
    implementation     "org.postgresql:postgresql:$postgres_version"
    jooqRuntime        "org.postgresql:postgresql:$postgres_version"
    implementation     "com.zaxxer:HikariCP:$hikari_version"
    implementation     "org.jooq:jooq:$jooq_version"
    implementation     "org.cache2k:cache2k-core:$cache2k_version"

    // logging and utility libraries
    implementation     "info.picocli:picocli:$picocli_version"
    implementation     "javax.annotation:javax.annotation-api:$javax_annotations_version"

    // dependency injection libraries
    implementation     "org.koin:koin-core:$koin_version"
    implementation     "org.koin:koin-core-ext:$koin_version"

    implementation 'com.squareup.okhttp3:okhttp:4.3.1'
    implementation 'com.auth0:java-jwt:3.9.0'

    compile group: 'org.apache.poi', name: 'poi', version: '4.1.2'
    compile group: 'org.apache.poi', name: 'poi-ooxml', version: '4.1.2'
    compile group: 'org.jboss.xnio', name: 'xnio-nio', version: '3.7.0.Final'

    compile group: 'com.fasterxml.jackson.core', name: 'jackson-core', version: '2.10.0'
    compile group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: '2.10.0'
    compile group: 'com.fasterxml.jackson.core', name: 'jackson-annotations', version: '2.10.0'

    // Testing libraries
    testImplementation "org.slf4j:slf4j-nop:$slf4j_version"
    testImplementation "org.mockito:mockito-core:$mockito_version"
    testImplementation "org.junit.jupiter:junit-jupiter-engine:$jupiter_version"
    testImplementation "io.kotlintest:kotlintest-runner-junit5:$kotlintest_version"
}


jooq {
    version = '3.12.3'
    edition = 'OSS'

    dev(sourceSets.main) {
        jdbc {
            driver = property('generation', 'generate.driver')
            url = property('generation', 'generate.uri')
            user = property('generation', 'generate.username')
            password = property('generation', 'generate.password')
        }

        generator {
            name = 'org.jooq.codegen.DefaultGenerator'
            strategy {
                name = 'org.jooq.codegen.DefaultGeneratorStrategy'
            }
            database {
                name = 'org.jooq.meta.postgres.PostgresDatabase'
                inputSchema = 'public'
            }
            generate {
                relations = true
                deprecated = false
                records = true
                immutablePojos = true
                fluentSetters = true
            }
            target {
                packageName = 'ru.hse.cppr.data.database_generated'
            }
        }
    }
}